# Game Theory Tutorial
## Game theory primer aimed at SWC PhD SNTN tutorials
To use, set up a python environment with nashpy included (for the last section). Open the .ipynb notebook file in Jupyter (to avoid bugs with VSCode's .ipynb extentions), and run. If there are any problems, restart the Jupyter kernel.

## Includes
- Introduction to backward induction using the Nim game
- History of Prisoner's Dilemma
- The Nashpy python module for defining games and looking at interaction between strategies
